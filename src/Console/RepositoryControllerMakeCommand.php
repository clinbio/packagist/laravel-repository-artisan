<?php

namespace Dterumal\RepositoryArtisan\Console;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Symfony\Component\Console\Input\InputOption;

class RepositoryControllerMakeCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'make:repository-controller';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new repository controller';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Class';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub(): string
    {
        $stub = '/stubs/controller.repository.stub';

        return $this->resolveStubPath($stub);
    }

    /**
     * Resolve the fully-qualified path to the stub.
     *
     * @param  string  $stub
     * @return string
     */
    protected function resolveStubPath(string $stub)
    {
        return file_exists($customPath = $this->laravel->basePath(trim($stub, '/')))
            ? $customPath
            : __DIR__.$stub;
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Http\Controllers';
    }

    /**
     * Build the class with the given name.
     *
     * Remove the base controller import if we are already in the base namespace.
     *
     * @param  string  $name
     * @return string
     * @throws FileNotFoundException
     */
    protected function buildClass($name)
    {
        $controllerNamespace = $this->getNamespace($name);

        $replace = [];

        $replace = $this->buildModelAndRequestsAndInterfaceReplacements($replace);

        $replace["use {$controllerNamespace}\Controller;\n"] = '';

        return str_replace(
            array_keys($replace), array_values($replace), parent::buildClass($name)
        );
    }

    /**
     * Build the model replacement values.
     *
     * @param  array  $replace
     * @return array
     */
    protected function buildModelAndRequestsAndInterfaceReplacements(array $replace)
    {
        $modelClass = $this->parseModel($this->option('model'));

        $interface = $this->parseInterface($this->option('interface'));

        $storeRequestClass = 'App\Http\Requests\Store'.$this->option('model').'Request';

        $updateRequestClass = 'App\Http\Requests\Update'.$this->option('model').'Request';

        if (! class_exists($modelClass)) {
            if ($this->confirm("A {$modelClass} model does not exist. Do you want to generate it?", true)) {
                $this->call('make:model', ['name' => $modelClass]);
            }
        }

        if (! interface_exists($interface)) {
            if ($this->confirm("A {$interface} interface does not exist. Do you want to generate it?", true)) {
                $this->call('make:interface', [
                    'name' => $interface,
                    '--model' => $modelClass,
                    '--extend' => 'BaseRepository'
                ]);
            }
        }

        if (! class_exists($storeRequestClass)) {
            if ($this->confirm("A {$storeRequestClass} request does not exist. Do you want to generate it?", true)) {
                $this->call('make:request', ['name' => $storeRequestClass]);
            }
        }

        if (! class_exists($updateRequestClass)) {
            if ($this->confirm("A {$updateRequestClass} request does not exist. Do you want to generate it?", true)) {
                $this->call('make:request', ['name' => $updateRequestClass]);
            }
        }

        return array_merge($replace, [
            '{{ namespacedModel }}' => $modelClass,
            '{{namespacedModel}}' => $modelClass,
            '{{ model }}' => class_basename($modelClass),
            '{{model}}' => class_basename($modelClass),
            '{{ modelVariable }}' => lcfirst(class_basename($modelClass)),
            '{{modelVariable}}' => lcfirst(class_basename($modelClass)),
            '{{ modelPlural }}' =>  Str::plural(lcfirst(class_basename($modelClass))),
            '{{modelPlural}}' => Str::plural(lcfirst(class_basename($modelClass))),
            '{{ namespacedStoreRequest }}' => $storeRequestClass,
            '{{namespacedStoreRequest}}' => $storeRequestClass,
            '{{ storeRequest }}' => class_basename($storeRequestClass),
            '{{storeRequest}}' => class_basename($storeRequestClass),
            '{{ namespacedUpdateRequest }}' => $updateRequestClass,
            '{{namespacedUpdateRequest}}' => $updateRequestClass,
            '{{ updateRequest }}' => class_basename($updateRequestClass),
            '{{updateRequest}}' => class_basename($updateRequestClass),
            '{{ namespacedInterface }}' => $interface,
            '{{namespacedInterface}}' => $interface,
            '{{ interface }}' => class_basename($interface),
            '{{interface}}' => class_basename($interface),
        ]);
    }

    /**
     * Get the fully-qualified model class name.
     *
     * @param  string  $model
     * @return string
     *
     * @throws InvalidArgumentException
     */
    protected function parseModel(string $model)
    {
        if (preg_match('([^A-Za-z0-9_/\\\\])', $model)) {
            throw new InvalidArgumentException('Model name contains invalid characters.');
        }

        return $this->qualifyModel($model);
    }

    /**
     * Get the fully-qualified interface name.
     *
     * @param  string  $interface
     * @return string
     *
     * @throws InvalidArgumentException
     */
    protected function parseInterface(string $interface)
    {
        if (preg_match('([^A-Za-z0-9_/\\\\])', $interface)) {
            throw new InvalidArgumentException('Interface name contains invalid characters.');
        }

        return $this->qualifyInterface($interface);
    }

    /**
     * Qualify the given model class base name.
     *
     * @param  string  $model
     * @return string
     */
    protected function qualifyInterface(string $model)
    {
        $model = ltrim($model, '\\/');

        $model = str_replace('/', '\\', $model);

        $rootNamespace = $this->rootNamespace();

        if (Str::startsWith($model, $rootNamespace)) {
            return $model;
        }

        return is_dir(app_path('Contracts'))
            ? $rootNamespace.'Contracts\\'.$model
            : $rootNamespace.$model;
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['force', null, InputOption::VALUE_NONE, 'Create the interface even if the interface already exists.'],
            ['interface', 'i', InputOption::VALUE_REQUIRED, 'The given interface to use in the controller'],
            ['model', 'm', InputOption::VALUE_REQUIRED, 'The given model to use in the controller.']
        ];
    }
}
