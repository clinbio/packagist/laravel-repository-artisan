<?php

namespace Dterumal\RepositoryArtisan\Console;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Symfony\Component\Console\Input\InputOption;

class RepositoryMakeCommand extends GeneratorCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:repository';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new repository class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Repository';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        $stub = null;

        if ($this->option('model')) {
            $stub = '/stubs/repository.model.stub';
        } elseif ($this->option('interface')) {
            $stub = '/stubs/repository.interface.stub';
        }

        if ($this->option('interface') && ! is_null($stub) && $this->option('model')) {
            $stub = str_replace('.stub', '.interface.stub', $stub);
        }

        if ($this->option('base') && ! is_null($stub) && $this->option('model')) {
            $stub = str_replace('.stub', '.base.stub', $stub);
        }

        $stub = $stub ?? '/stubs/repository.plain.stub';

        return $this->resolveStubPath($stub);
    }

    /**
     * Resolve the fully-qualified path to the stub.
     *
     * @param  string  $stub
     * @return string
     */
    protected function resolveStubPath(string $stub)
    {
        return file_exists($customPath = $this->laravel->basePath(trim($stub, '/')))
            ? $customPath
            : __DIR__.$stub;
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Repositories';
    }

    /**
     * Build the class with the given name.
     *
     * Remove the base controller import if we are already in the base namespace.
     *
     * @param  string  $name
     * @return string
     * @throws FileNotFoundException
     */
    protected function buildClass($name)
    {
        $replace = [];

        if ($this->option('model')) {
            $replace = $this->buildModelReplacements($replace);
        }

        if ($this->option('interface')) {
            $replace = $this->buildInterfaceReplacements($replace);
        }

        if ($this->option('base')) {
            $replace = $this->buildBaseInterfaceReplacements($replace);
        }

        return str_replace(
            array_keys($replace), array_values($replace), parent::buildClass($name)
        );
    }

    /**
     * Build the model replacement values.
     *
     * @param  array  $replace
     * @return array
     */
    protected function buildInterfaceReplacements(array $replace)
    {
        $interfaceClass = $this->parseInterface($this->option('interface'));

        $modelClass = null;

        if ($this->option('model')) {
            $modelClass = $this->parseModel($this->option('model'));
        }

        if (! interface_exists($interfaceClass)) {
            if ($this->confirm("A {$interfaceClass} interface does not exist. Do you want to generate it?", true)) {
                if ($modelClass) {
                    $this->call('make:interface', [
                        'name' => $interfaceClass,
                        '--model' => $modelClass
                    ]);
                } else {
                    $this->call('make:interface', [
                        'name' => $interfaceClass
                    ]);
                }
            }
        }

        return array_merge($replace, [
            '{{ namespacedInterface }}' => $interfaceClass,
            '{{namespacedInterface}}' => $interfaceClass,
            '{{ interface }}' => class_basename($interfaceClass),
            '{{interface}}' => class_basename($interfaceClass)
        ]);
    }

    /**
     * Build the model replacement values.
     *
     * @param  array  $replace
     * @return array
     */
    protected function buildBaseInterfaceReplacements(array $replace)
    {
        $interfaceClass = $this->parseInterface($this->option('base'));

        $modelClass = null;

        if ($this->option('model')) {
            $modelClass = $this->parseModel($this->option('model'));
        }

        if (! interface_exists($interfaceClass)) {
            if ($this->confirm("A {$interfaceClass} interface does not exist. Do you want to generate it?", true)) {
                if ($modelClass) {
                    $this->call('make:interface', [
                        'name' => $interfaceClass,
                        '--model' => $modelClass,
                        '--base' => null
                    ]);
                } else {
                    $this->call('make:interface', [
                        'name' => $interfaceClass
                    ]);
                }
            }
        }

        return array_merge($replace, [
            '{{ namespacedInterface }}' => $interfaceClass,
            '{{namespacedInterface}}' => $interfaceClass,
            '{{ interface }}' => class_basename($interfaceClass),
            '{{interface}}' => class_basename($interfaceClass)
        ]);
    }

    /**
     * Build the model replacement values.
     *
     * @param  array  $replace
     * @return array
     */
    protected function buildModelReplacements(array $replace)
    {
        $modelClass = $this->parseModel($this->option('model'));

        if (! class_exists($modelClass)) {
            if ($this->confirm("A {$modelClass} model does not exist. Do you want to generate it?", true)) {
                $this->call('make:model', ['name' => $modelClass]);
            }
        }

        return array_merge($replace, [
            '{{ namespacedModel }}' => $modelClass,
            '{{namespacedModel}}' => $modelClass,
            '{{ model }}' => class_basename($modelClass),
            '{{model}}' => class_basename($modelClass),
            '{{ modelVariable }}' => lcfirst(class_basename($modelClass)),
            '{{modelVariable}}' => lcfirst(class_basename($modelClass)),
        ]);
    }

    /**
     * Get the fully-qualified model class name.
     *
     * @param  string  $model
     * @return string
     *
     * @throws InvalidArgumentException
     */
    protected function parseModel(string $model)
    {
        if (preg_match('([^A-Za-z0-9_/\\\\])', $model)) {
            throw new InvalidArgumentException('Model name contains invalid characters.');
        }

        return $this->qualifyModel($model);
    }

    /**
     * Get the fully-qualified model class name.
     *
     * @param  string  $interface
     * @return string
     *
     * @throws InvalidArgumentException
     */
    protected function parseInterface(string $interface)
    {
        if (preg_match('([^A-Za-z0-9_/\\\\])', $interface)) {
            throw new InvalidArgumentException('Interface name contains invalid characters.');
        }

        return $this->qualifyInterface($interface);
    }

    /**
     * Qualify the given model class base name.
     *
     * @param  string  $interface
     * @return string
     */
    protected function qualifyInterface(string $interface)
    {
        $interface = ltrim($interface, '\\/');

        $interface = str_replace('/', '\\', $interface);

        $rootNamespace = $this->rootNamespace();

        if (Str::startsWith($interface, $rootNamespace)) {
            return $interface;
        }

        return is_dir(app_path('Contracts'))
            ? $rootNamespace.'Contracts\\'.$interface
            : $rootNamespace.$interface;
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['force', null, InputOption::VALUE_NONE, 'Create the repository even if the repository already exists.'],
            ['base', 'b', InputOption::VALUE_OPTIONAL, 'Generate a repository class which implements the given base interface.'],
            ['interface', 'i', InputOption::VALUE_OPTIONAL, 'Generate a repository class which implements the given interface.'],
            ['model', 'm', InputOption::VALUE_OPTIONAL, 'Generate a repository class for the given model.']
        ];
    }
}
