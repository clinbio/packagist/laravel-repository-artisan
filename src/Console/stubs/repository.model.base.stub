<?php

namespace {{ namespace }};

use {{ namespacedInterface }};
use {{ namespacedModel }};
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class {{ class }} implements {{ interface }}
{
    /**
     * @inheritDoc
     */
    public function all(Request $request): LengthAwarePaginator
    {
        $itemsPerPage = $request->input('items-per-page', 10);

        return {{ model }}::query()
            ->paginate($itemsPerPage);
    }

    /**
     * @inheritDoc
     */
    public function get(Request $request): Collection
    {
        return {{ model }}::query()
            ->get();
    }

    /**
     * @inheritDoc
     */
    public function find(Model $model): Model
    {
        return $model;
    }

    /**
     * @inheritDoc
     */
    public function store(array $request): Model
    {
        return {{ model }}::create($request);
    }

    /**
     * @inheritDoc
     */
    public function update(array $request, Model $model): Model
    {
        $model->fill($request)->save();
        return $model->fresh();
    }

    /**
     * @inheritDoc
     */
    public function delete(Model $model): bool
    {
        return $model->delete();
    }

    /**
     * @inheritDoc
     */
    public function destroy(Model $model): bool
    {
        return $model->forceDelete();
    }
}