<?php

namespace Dterumal\RepositoryArtisan;

use Dterumal\RepositoryArtisan\Console\InterfaceMakeCommand;
use Dterumal\RepositoryArtisan\Console\RepositoryControllerMakeCommand;
use Dterumal\RepositoryArtisan\Console\RepositoryMakeCommand;
use Illuminate\Support\ServiceProvider;

class RepositoryArtisanServiceProvider extends ServiceProvider
{
    public function register()
    {
        //
    }

    public function boot()
    {
        $this->commands([
            RepositoryMakeCommand::class,
            InterfaceMakeCommand::class,
            RepositoryControllerMakeCommand::class
        ]);
    }
}