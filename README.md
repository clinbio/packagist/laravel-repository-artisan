<p align="center"><img src="https://banners.beyondco.de/Laravel%20Repository%20Artisan.png?theme=light&packageManager=composer+require&packageName=dterumal%2Flaravel-repository-artisan&pattern=architect&style=style_1&description=Some+useful+Artisan+commands+for+repository+pattern+in+Laravel&md=1&showWatermark=1&fontSize=100px&images=folder-open"></p>

## Installation

You can install the package via composer:

```bash
composer require dterumal/laravel-repository-artisan

```

## Usage

```php
php artisan make:interface --model=Foo FooInterface
```

This will create an interface `FooInterface` in `App/Contracts` directory using the `Foo` Eloquent model.

```php
php artisan make:interface --base FooInterface
```

This will create a base interface `FooInterface` in `App/Contracts` directory which implements the classic CRUD methods. Pleas note that it will use `Illuminate\Database\Eloquent\Model` instead of a specific model.

```php
php artisan make:interface --extend=FooBase FooInterface
```

This will create an interface `FooInterface` which extends the `FooBase` interface.

```php
php artisan make:repository --model=Foo --interface=FooInterface FooRepository
```

This will create a repository class `FooRepository` implementing the interface `FooInterface` and using `Foo` Eloquent model.

```php
php artisan make:repository --model=Foo --base=FooInterface FooRepository
```

This will create a repository class `FooRepository` implementing the base interface `FooInterface` and using `Foo` Eloquent model.

```php
php artisan make:repository-controller --model=Foo --interface=FooInterface FooController
```

This will create a controller `FooController` which will inject the give interface `FooInterface` and bind the given model `Foo`

**Note:** When using options such as `--model` or `--interface`, please note that if class/interface doesn't exist, the terminal will prompt you with a confirmation to automatically create it.

## Testing

```bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security Vulnerabilities

Please review [our security policy](.gitlab/SECURITY.md) on how to report security vulnerabilities.

## Credits

- [Dillenn Terumalai](https://gitlab.sib.swiss/dterumal)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
