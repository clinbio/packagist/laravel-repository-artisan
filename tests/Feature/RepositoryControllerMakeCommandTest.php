<?php

namespace Dterumal\RepositoryArtisan\Tests\Feature;

use Dterumal\RepositoryArtisan\Tests\TestCase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;

class RepositoryControllerMakeCommandTest extends TestCase
{
    /** @test */
    function it_creates_a_new_repository_controller()
    {
        // destination path of the Foo model
        $fooModel = app_path('Models/Foo.php');

        // destination path of the MyFoo interface
        $fooInterface = app_path('Contracts/MyFooInterface.php');

        // destination path of the MyFoo controller
        $fooController = app_path('Http/Controllers/MyFooController.php');

        // make sure we're starting from a clean state
        if (File::exists($fooController)) {
            unlink($fooController);
        }
        if (File::exists($fooModel)) {
            unlink($fooModel);
        }
        if (File::exists($fooInterface)) {
            unlink($fooInterface);
        }

        $this->assertFalse(File::exists($fooController));
        $this->assertFalse(File::exists($fooModel));
        $this->assertFalse(File::exists($fooInterface));

        // Run the make command
        Artisan::call('make:repository-controller MyFooController --model=Foo --interface=MyFooInterface --no-interaction');

        // Assert a new file is created
        $this->assertTrue(File::exists($fooController));
        $this->assertTrue(File::exists($fooModel));
        $this->assertTrue(File::exists($fooInterface));

        // Assert the file contains the right contents
        $expectedContents = <<<CLASS
<?php

namespace App\Http\Controllers;

use App\Contracts\MyFooInterface;
use App\Http\Requests\StoreFooRequest;
use App\Http\\Requests\UpdateFooRequest;
use App\Models\Foo;
use Illuminate\Http\Request;
use Inertia\Inertia;

class MyFooController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request \$request
     * @return \Illuminate\Http\JsonResponse|\Inertia\Response
     */
    public function index(Request \$request)
    {
        return \$request->wantsJson()
            ? response()->json(app(MyFooInterface::class)->get())
            : Inertia::render('Path/To/Page', [
                'foos' => app(MyFooInterface::class)->all(\$request),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreFooRequest \$request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function store(StoreFooRequest \$request)
    {
        \$validated = \$request->validated();

        \$foo = app(MyFooInterface::class)->store(\$validated);

        return \$request->wantsJson()
            ?  response()->json(\$foo)
            : back()->with('foo-stored');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request \$request
     * @param  \App\Models\Foo \$foo
     * @return \Illuminate\Http\JsonResponse|\Inertia\Response
     */
    public function show(Request \$request, Foo \$foo)
    {
        return \$request->wantsJson()
            ? response()->json(app(MyFooInterface::class)->find(\$foo))
            : Inertia::render('Path/To/Page', [
                'foo' => app(MyFooInterface::class)->find(\$foo)
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateFooRequest  \$request
     * @param  \App\Models\Foo \$foo
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function update(UpdateFooRequest \$request, Foo \$foo)
    {
        \$validated = \$request->validated();

        \$foo = app(MyFooInterface::class)->update(\$validated, \$foo);

        return \$request->wantsJson()
            ?  response()->json(\$foo)
            : back()->with('foo-updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request \$request
     * @param  \App\Models\Foo \$foo
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function destroy(Request \$request, Foo \$foo)
    {
        if (\$foo->trashed()) {
            \$isDeleted = app(MyFooInterface::class)->destroy(\$foo);
            return \$request->wantsJson()
                ?  response()->json(\$isDeleted)
                : back()->with('foo-removed');
        }

        \$isDeleted = app(MyFooInterface::class)->delete(\$foo);
        return \$request->wantsJson()
            ?  response()->json(\$isDeleted)
            : back()->with('foo-deleted');
    }
}
CLASS;

        $this->assertEquals($expectedContents, file_get_contents($fooController));
    }
}