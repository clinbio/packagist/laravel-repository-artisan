<?php

namespace Dterumal\RepositoryArtisan\Tests\Feature;

use Dterumal\RepositoryArtisan\Tests\TestCase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;

class RepositoryMakeCommandTest extends TestCase
{
    /** @test */
    function it_creates_a_new_plain_repository()
    {
        // destination path of the Foo class
        $fooRepository = app_path('Repositories/MyFooRepository.php');

        // make sure we're starting from a clean state
        if (File::exists($fooRepository)) {
            unlink($fooRepository);
        }

        $this->assertFalse(File::exists($fooRepository));

        // Run the make command
        Artisan::call('make:repository MyFooRepository');

        // Assert a new file is created
        $this->assertTrue(File::exists($fooRepository));

        // Assert the file contains the right contents
        $expectedContents = <<<CLASS
<?php

namespace App\Repositories;

class MyFooRepository
{
    //
}
CLASS;

        $this->assertEquals($expectedContents, file_get_contents($fooRepository));
    }

    /** @test */
    function it_creates_a_new_model_repository()
    {
        // destination path of the Foo class
        $fooRepository = app_path('Repositories/MyFooRepository.php');

        // destination path of the Foo model
        $fooModel = app_path('Models/Foo.php');

        // make sure we're starting from a clean state
        if (File::exists($fooRepository)) {
            unlink($fooRepository);
        }
        if (File::exists($fooModel)) {
            unlink($fooModel);
        }

        $this->assertFalse(File::exists($fooRepository));
        $this->assertFalse(File::exists($fooModel));

        // Run the make command
        Artisan::call('make:repository MyFooRepository --model=Foo --no-interaction');

        // Assert a new file is created
        $this->assertTrue(File::exists($fooRepository));
        $this->assertTrue(File::exists($fooModel));

        // Assert the file contains the right contents
        $expectedContents = <<<CLASS
<?php

namespace App\Repositories;

use App\Models\Foo;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class MyFooRepository
{
    /**
     * Returns a paginated listing of the resource.
     *
     * @param \Illuminate\Http\Request  \$request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function all(Request \$request): LengthAwarePaginator
    {
        \$itemsPerPage = \$request->input('items-per-page', 10);

        return Foo::query()
            ->paginate(\$itemsPerPage);
    }

    /**
     * Returns a listing of the resource.
     *
     * @param \Illuminate\Http\Request  \$request
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function get(Request \$request): Collection
    {
        return Foo::query()
            ->get();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Foo  \$foo
     * @return \App\Models\Foo
     */
        public function find(Foo \$foo): Foo
    {
        return \$foo;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  array  \$request
     * @return \App\Models\Foo  \$foo
     */
    public function store(array \$request): Foo
    {
        return Foo::create(\$request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  array  \$request
     * @param  \App\Models\Foo  \$foo
     * @return \App\Models\Foo
     */
    public function update(array \$request, Foo \$foo): Foo
    {
        \$foo->fill(\$request)->save();
        return \$foo;
    }

    /**
     * Delete the specified resource from storage.
     *
     * @param  \App\Models\Foo  \$foo
     * @return bool
     */
    public function delete(Foo \$foo): bool
    {
        return \$foo->delete();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Foo  \$foo
     * @return bool
     */
    public function destroy(Foo \$foo): bool
    {
        return \$foo->forceDelete();
    }
}
CLASS;

        $this->assertEquals($expectedContents, file_get_contents($fooRepository));
    }

    /** @test */
    function it_creates_a_new_interface_repository()
    {
        // destination path of the Foo class
        $fooRepository = app_path('Repositories/MyFooRepository.php');

        // destination path of the MyFoo interface
        $fooInterface = app_path('Contracts/MyFooInterface.php');

        // make sure we're starting from a clean state
        if (File::exists($fooRepository)) {
            unlink($fooRepository);
        }
        if (File::exists($fooInterface)) {
            unlink($fooInterface);
        }

        $this->assertFalse(File::exists($fooRepository));
        $this->assertFalse(File::exists($fooInterface));

        // Run the make command
        Artisan::call('make:repository MyFooRepository --interface=MyFooInterface --no-interaction');

        // Assert a new file is created
        $this->assertTrue(File::exists($fooRepository));
        $this->assertTrue(File::exists($fooInterface));

        // Assert the file contains the right contents
        $expectedContents = <<<CLASS
<?php

namespace App\Repositories;

use App\Contracts\MyFooInterface;

class MyFooRepository implements MyFooInterface
{
    //
}
CLASS;

        $this->assertEquals($expectedContents, file_get_contents($fooRepository));
    }

    /** @test */
    function it_creates_a_new_model_interface_repository()
    {
        // destination path of the Foo class
        $fooRepository = app_path('Repositories/MyFooRepository.php');

        // destination path of the Foo model
        $fooModel = app_path('Models/Foo.php');

        // destination path of the MyFoo interface
        $fooInterface = app_path('Contracts/MyFooInterface.php');

        // make sure we're starting from a clean state
        if (File::exists($fooRepository)) {
            unlink($fooRepository);
        }
        if (File::exists($fooModel)) {
            unlink($fooModel);
        }
        if (File::exists($fooInterface)) {
            unlink($fooInterface);
        }

        $this->assertFalse(File::exists($fooRepository));
        $this->assertFalse(File::exists($fooModel));
        $this->assertFalse(File::exists($fooInterface));

        // Run the make command
        Artisan::call('make:repository MyFooRepository --model=Foo --interface=MyFooInterface --no-interaction');

        // Assert a new file is created
        $this->assertTrue(File::exists($fooRepository));
        $this->assertTrue(File::exists($fooModel));
        $this->assertTrue(File::exists($fooInterface));

        // Assert the file contains the right contents
        $expectedContents = <<<CLASS
<?php

namespace App\Repositories;

use App\Contracts\MyFooInterface;
use App\Models\Foo;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class MyFooRepository implements MyFooInterface
{
    /**
     * @inheritDoc
     */
    public function all(Request \$request): LengthAwarePaginator
    {
        \$itemsPerPage = \$request->input('items-per-page', 10);

        return Foo::query()
            ->paginate(\$itemsPerPage);
    }

    /**
     * @inheritDoc
     */
    public function get(Request \$request): Collection
    {
        return Foo::query()
            ->get();
    }

    /**
     * @inheritDoc
     */
    public function find(Foo \$foo): Foo
    {
        return \$foo;
    }

    /**
     * @inheritDoc
     */
    public function store(array \$request): Foo
    {
        return Foo::create(\$request);
    }

    /**
     * @inheritDoc
     */
    public function update(array \$request, Foo \$foo): Foo
    {
        \$foo->fill(\$request)->save();
        return \$foo->fresh();
    }

    /**
     * @inheritDoc
     */
    public function delete(Foo \$foo): bool
    {
        return \$foo->delete();
    }

    /**
     * @inheritDoc
     */
    public function destroy(Foo \$foo): bool
    {
        return \$foo->forceDelete();
    }
}
CLASS;

        $this->assertEquals($expectedContents, file_get_contents($fooRepository));
    }

    /** @test */
    function it_creates_a_new_model_base_repository()
    {
        // destination path of the Foo class
        $fooRepository = app_path('Repositories/MyFooRepository.php');

        // destination path of the Foo model
        $fooModel = app_path('Models/Foo.php');

        // destination path of the MyFoo interface
        $fooInterface = app_path('Contracts/FooInterface.php');

        // make sure we're starting from a clean state
        if (File::exists($fooRepository)) {
            unlink($fooRepository);
        }
        if (File::exists($fooModel)) {
            unlink($fooModel);
        }
        if (File::exists($fooInterface)) {
            unlink($fooInterface);
        }

        $this->assertFalse(File::exists($fooRepository));
        $this->assertFalse(File::exists($fooModel));
        $this->assertFalse(File::exists($fooInterface));

        // Run the make command
        Artisan::call('make:repository MyFooRepository --model=Foo --base=FooInterface --no-interaction');

        // Assert a new file is created
        $this->assertTrue(File::exists($fooRepository));
        $this->assertTrue(File::exists($fooModel));
        $this->assertTrue(File::exists($fooInterface));

        // Assert the file contains the right contents
        $expectedContents = <<<CLASS
<?php

namespace App\Repositories;

use App\Contracts\FooInterface;
use App\Models\Foo;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class MyFooRepository implements FooInterface
{
    /**
     * @inheritDoc
     */
    public function all(Request \$request): LengthAwarePaginator
    {
        \$itemsPerPage = \$request->input('items-per-page', 10);

        return Foo::query()
            ->paginate(\$itemsPerPage);
    }

    /**
     * @inheritDoc
     */
    public function get(Request \$request): Collection
    {
        return Foo::query()
            ->get();
    }

    /**
     * @inheritDoc
     */
    public function find(Model \$model): Model
    {
        return \$model;
    }

    /**
     * @inheritDoc
     */
    public function store(array \$request): Model
    {
        return Foo::create(\$request);
    }

    /**
     * @inheritDoc
     */
    public function update(array \$request, Model \$model): Model
    {
        \$model->fill(\$request)->save();
        return \$model->fresh();
    }

    /**
     * @inheritDoc
     */
    public function delete(Model \$model): bool
    {
        return \$model->delete();
    }

    /**
     * @inheritDoc
     */
    public function destroy(Model \$model): bool
    {
        return \$model->forceDelete();
    }
}
CLASS;

        $this->assertEquals($expectedContents, file_get_contents($fooRepository));
    }
}