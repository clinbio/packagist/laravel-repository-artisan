<?php

namespace Dterumal\RepositoryArtisan\Tests\Feature;

use Dterumal\RepositoryArtisan\Tests\TestCase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;

class InterfaceMakeCommandTest extends TestCase
{
    /** @test */
    function it_creates_a_new_plain_interface()
    {
        // destination path of the Foo class
        $fooInterface = app_path('Contracts/MyFooInterface.php');

        // make sure we're starting from a clean state
        if (File::exists($fooInterface)) {
            unlink($fooInterface);
        }

        $this->assertFalse(File::exists($fooInterface));

        // Run the make command
        Artisan::call('make:interface MyFooInterface');

        // Assert a new file is created
        $this->assertTrue(File::exists($fooInterface));

        // Assert the file contains the right contents
        $expectedContents = <<<CLASS
<?php

namespace App\Contracts;

interface MyFooInterface
{
    //
}
CLASS;

        $this->assertEquals($expectedContents, file_get_contents($fooInterface));
    }

    /** @test */
    function it_creates_a_new_extend_interface()
    {
        // destination path of the Foo class
        $fooInterface = app_path('Contracts/MyFooInterface.php');

        // make sure we're starting from a clean state
        if (File::exists($fooInterface)) {
            unlink($fooInterface);
        }

        $this->assertFalse(File::exists($fooInterface));

        // Run the make command
        Artisan::call('make:interface MyFooInterface --extend BaseRepository');

        // Assert a new file is created
        $this->assertTrue(File::exists($fooInterface));

        // Assert the file contains the right contents
        $expectedContents = <<<CLASS
<?php

namespace App\Contracts;

interface MyFooInterface extends App\Contracts\BaseRepository
{
    //
}
CLASS;

        $this->assertEquals($expectedContents, file_get_contents($fooInterface));
    }

    /** @test */
    function it_creates_a_new_model_interface()
    {
        // destination path of the Foo class
        $fooInterface = app_path('Contracts/MyFooInterface.php');

        // destination path of the Foo model
        $fooModel = app_path('Models/Foo.php');

        // make sure we're starting from a clean state
        if (File::exists($fooInterface)) {
            unlink($fooInterface);
        }
        if (File::exists($fooModel)) {
            unlink($fooModel);
        }

        $this->assertFalse(File::exists($fooInterface));
        $this->assertFalse(File::exists($fooModel));

        // Run the make command
        Artisan::call('make:interface MyFooInterface --model=Foo --no-interaction');

        // Assert a new file is created
        $this->assertTrue(File::exists($fooInterface));
        $this->assertTrue(File::exists($fooModel));

        // Assert the file contains the right contents
        $expectedContents = <<<CLASS
<?php

namespace App\Contracts;

use App\Models\Foo;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

interface MyFooInterface
{
    /**
     * Returns a paginated listing of the resource.
     *
     * @param  \Illuminate\Http\Request  \$request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator;
     */
    public function all(Request \$request): LengthAwarePaginator;

    /**
     * Returns a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  \$request
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function get(Request \$request): Collection;

    /**
     * Store a newly created resource in storage.
     *
     * @param  array  \$request
     * @return \App\Models\Foo  \$foo
     */
    public function store(array \$request): Foo;

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Foo  \$foo
     * @return \App\Models\Foo
     */
    public function find(Foo \$foo): Foo;

    /**
     * Update the specified resource in storage.
     *
     * @param  array  \$request
     * @param  \App\Models\Foo  \$foo
     * @return \App\Models\Foo
     */
    public function update(array \$request, Foo \$foo): Foo;

    /**
     * Delete the specified resource in storage.
     *
     * @param  \App\Models\Foo  \$foo
     * @return bool
     */
    public function delete(Foo \$foo): bool;

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Foo  \$foo
     * @return bool
     */
    public function destroy(Foo \$foo): bool;
}
CLASS;

        $this->assertEquals($expectedContents, file_get_contents($fooInterface));
    }

    /** @test */
    function it_creates_a_new_base_interface()
    {
        // destination path of the Foo class
        $fooInterface = app_path('Contracts/MyFooInterface.php');

        // make sure we're starting from a clean state
        if (File::exists($fooInterface)) {
            unlink($fooInterface);
        }

        $this->assertFalse(File::exists($fooInterface));

        // Run the make command
        Artisan::call('make:interface MyFooInterface --base --no-interaction');

        // Assert a new file is created
        $this->assertTrue(File::exists($fooInterface));

        // Assert the file contains the right contents
        $expectedContents = <<<CLASS
<?php

namespace App\Contracts;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

interface MyFooInterface
{
    /**
     * Returns a paginated listing of the resource.
     *
     * @param  \Illuminate\Http\Request  \$request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function all(Request \$request): LengthAwarePaginator;

    /**
     * Returns a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  \$request
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function get(Request \$request): Collection;

    /**
     * Store a newly created resource in storage.
     *
     * @param  array  \$request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function store(array \$request): Model;

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Database\Eloquent\Model  \$model
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function find(Model \$model): Model;

    /**
     * Update the specified resource in storage.
     *
     * @param  array  \$request
     * @param  \Illuminate\Database\Eloquent\Model  \$model
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update(array \$request, Model \$model): Model;

    /**
     * Delete the specified resource in storage.
     *
     * @param  \Illuminate\Database\Eloquent\Model  \$model
     * @return bool
     */
    public function delete(Model \$model): bool;

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Database\Eloquent\Model  \$model
     * @return bool
     */
    public function destroy(Model \$model): bool;
}
CLASS;

        $this->assertEquals($expectedContents, file_get_contents($fooInterface));
    }
}