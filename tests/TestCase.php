<?php

namespace Dterumal\RepositoryArtisan\Tests;

use Dterumal\RepositoryArtisan\RepositoryArtisanServiceProvider;

class TestCase extends \Orchestra\Testbench\TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        // additional setup
    }

    protected function getPackageProviders($app)
    {
        return [
            RepositoryArtisanServiceProvider::class,
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        // perform environment setup
    }
}